<%-- 
    Document   : tt
    Created on : 05.08.2014, 18:07:02
    Author     : fazero
--%>

<%@page import="java.util.List"%>
<%@page import="bookrental.BookHelper"%>
<%@page import="bookrental.Book"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <div>
            <%!
                BookHelper bookHelper = new BookHelper();
                long count = bookHelper.getBookCount();
                List titles = bookHelper.getBookTitles(1, 12);
                Book book = (Book) titles.get(1);
            %>
            <%= count %>
            <%= book.getTitle() %>
        </div>
    </body>
</html>
