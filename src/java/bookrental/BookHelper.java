/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookrental;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fazero
 */
public class BookHelper {

    Session session = null;

    public BookHelper() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }

    public List getBookTitles(int startId, int endId) {
        List<Book> titlesList = null;
        int count = (int) getBookCount();
        if ((startId <= endId) && (startId <= count)) { // @todo Предполагаю: данный функционал надо оформить в виде исключений + повторяющийся кусок
            if (startId <= 0) {
                startId = 1;
            }
            if (endId > count) {
                endId = count;
            }
            try {
                org.hibernate.Transaction tx = session.beginTransaction();
                Query q = session.createQuery("from Book as book where book.bookId between '" + startId + "' and '" + endId + "'");
                titlesList = (List<Book>) q.list();
                tx.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return titlesList;
    }

    public long getBookCount() {
        long count = 0;
        try {
            Query query = session.createQuery("select count(*) from Book");
            count = (Long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public void addBook(Book book) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            session.save(book);
            tx.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
