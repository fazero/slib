/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bookrental;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
/**
 *
 * @author fazero
 */
public class ReaderHelper {
    
    Session session = null;
    
    public ReaderHelper() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }
    
    public List getReaders(int startId, int endId) {
        List<Reader> readers = null;
        int count = (int) getReaderCount(); // @todo Возможно излишнее преобразование типов
        if ((startId <= endId) && (startId <= count)) { // @todo Предполагаю: данный функционал надо оформить в виде исключений + повторяющийся кусок
            if (startId <= 0) {
                startId = 1;
            }
            if (endId > count) {
                endId = count;
            }
            try {
                org.hibernate.Transaction tx = session.beginTransaction();
                Query q = session.createQuery("from Reader as reader where reader.readerId between '" + startId + "' and '" + endId + "'");
                readers = (List<Reader>) q.list();
                tx.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return readers;
    }
    
    public long getReaderCount() {
        long count = 0;
        try {
            Query query = session.createQuery("select count(*) from Reader");
            count = (Long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}
