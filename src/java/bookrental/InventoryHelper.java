/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookrental;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author fazero
 */
public class InventoryHelper {

    Session session = null;

    public InventoryHelper() {
        this.session = HibernateUtil.getSessionFactory().openSession();
    }

    public List getInventory(int startId, int endId) {
        List<Inventory> inventoryList = null;
        int count = (int) getInventoryAmount();
        if ((startId <= endId) && (startId <= count)) { // Думаю лучше было бы это оформить в виде исключений, но пока как.
            if (startId <= 0) {
                startId = 1;
            }
            if (endId > count) {
                endId = count;
            }
            try {
                org.hibernate.Transaction tx = session.beginTransaction();
                Query q = session.createQuery("from Inventory as inventory where inventory.inventoryId between '" + startId + "' and '" + endId + "'");
                inventoryList = (List<Inventory>) q.list();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return inventoryList;
    }

    public long getInventoryAmount() {
        long count = 0;
        try {
            Query query = session.createQuery("select count(*) from Inventory");
            count = (Long) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}
