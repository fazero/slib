/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bookrental;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import java.util.List;

/**
 *
 * @author fazero
 */
@WebService(serviceName = "SLibService")
@Stateless()
public class SLibService {

    /**
     * Операция веб-службы
     *
     * @param count
     * @param startId
     * @return
     */
    @WebMethod(operationName = "books")
    public List<Book> books(@WebParam(name = "count") int count, @WebParam(name = "startId") int startId) {
        BookHelper helper = new BookHelper();
        List<Book> bookTitles = null;
        bookTitles = (List<Book>) helper.getBookTitles(startId, startId + count);
        return bookTitles;
    }

    /**
     * Операция веб-службы
     */
    @WebMethod(operationName = "readers")
    public List<Reader> readers(@WebParam(name = "count") int count, @WebParam(name = "startId") int startId) {
        ReaderHelper helper = new ReaderHelper();
        List<Reader> readers = null;
        readers = (List<Reader>) helper.getReaders(startId, startId + count);
        return readers;
    }

    /**
     * Операция веб-службы
     */
    @WebMethod(operationName = "addBook")
    public int addBook(@WebParam(name = "book") Book book) {
        int newBookId = 0;
        try {
            BookHelper helper = new BookHelper();
            helper.addBook(book);
            newBookId = book.getBookId();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return newBookId;
    }

    /**
     * Операция веб-службы
     */
    @WebMethod(operationName = "addReader")
    public int addReader(@WebParam(name = "reader") Reader reader) {
        //TODO write your implementation code here:
        return 0;
    }

    /**
     * Операция веб-службы
     */
    @WebMethod(operationName = "takeBook")
    public int takeBook(@WebParam(name = "rental") Rental rental) {
        //TODO write your implementation code here:
        return 0;
    }

    /**
     * Операция веб-службы
     */
    @WebMethod(operationName = "releaseBook")
    public Boolean releaseBook(@WebParam(name = "rental") Rental rental) {
        //TODO write your implementation code here:
        return null;
    }

    /**
     * Операция веб-службы
     *
     * @return
     */
    @WebMethod(operationName = "count")
    public Integer count() {
        BookHelper bookHelper = new BookHelper();
        Integer k = (Integer) 100;
        return k;
    }

    /**
     * Операция веб-службы
     *
     * @param i
     * @param j
     * @return
     */
    @WebMethod(operationName = "add")
    public int add(@WebParam(name = "i") int i, @WebParam(name = "j") int j) {
        int k = i + j;
        return k;
    }
}
